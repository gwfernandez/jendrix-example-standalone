package com.jendrix.example.patterns.adapter;

public class ImpresoraExcelAdapter extends Impresora {

	private ImpresoraExcel impresoraExcel;

	public ImpresoraExcelAdapter() {
		super();
		impresoraExcel = new ImpresoraExcel();
	}

	@Override
	public void imprimir() {
		impresoraExcel.generarFichero(getTexto());
	}

}