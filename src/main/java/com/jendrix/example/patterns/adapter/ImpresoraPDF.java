package com.jendrix.example.patterns.adapter;

public class ImpresoraPDF extends Impresora {

	public ImpresoraPDF(String texto) {
		super();
		setTexto(texto);
	}

	@Override
	public void imprimir() {
		System.out.println("fichero pdf con " + getTexto());
	}

}