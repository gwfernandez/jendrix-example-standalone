package com.jendrix.example.patterns.adapter;

public class ImpresoraTexto extends Impresora {

	public ImpresoraTexto(String texto) {
		super();
		setTexto(texto);
	}

	@Override
	public void imprimir() {
		System.out.println("fichero texto con " + getTexto());
	}
}